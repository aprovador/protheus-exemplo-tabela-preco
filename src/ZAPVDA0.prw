#include "Totvs.ch"

/*
 * Exemplo de customizacao para aprovacao especifica de Tabelas de Preco (DA0) no Aprovador.
 *
 * Pre-requisitos de dicionario:
 *   - DA0_ZSTATU: campo do tipo caracter com os possiveis valores ('': Pendente, 'A': Aprovado, 'R': Reprovado)
 *   - DA0_ZAPROV: campo do tipo caracter com o login do usuario que sera o aprovador.
 *   - DA0_ZOBSM.: campo do tipo memo para armazenar a observacao da aprovacao/reprovacao.
 *
 * NOTE: Esta customizacao nao inclui validacoes adicionais no Protheus para impedir o uso de 
 *       tabelas de precos nao autorizadas. O Objetivo deste exemplo � mostrar como aprovacoes 
 *       especificas do Protheus podem ser enviadas para o Aprovador.
 *       As queries estao constru�das para banco de dados SQL Server e podem ser alteradas.
 */

// User Function chamada pelo Aprovador para buscar documentos pendentes. Para melhor performance,
// utilize os parametros de paginacao.
// 
// - Deve ser parametrizada no Aprovador Conector.
//
// Parametros:
//   - dSinceDate.....: data a partir de quando as pendencias serao buscadas.
//   - nLastId........: [PAGINACAO] R_E_C_N_O_ da ultima SC retornada pela pesquisa anterior. Ou seja,
//                      retorne os registros com R_E_C_N_O_ maiores do que este valor.
//   - nLimit.........: [PAGINACAO] Quantidade maxima de registros a serem retornados.
//   - @nRefLastId....: [PAGINACAO] Deve ser setado com o ultimo RECNO encontrado na pesquisa.
//   - @cRefError.....: caso ocorra algum erro na funcao, deve ser setado nesta variavel.
//
//   - Return: array com dados dos documentos (DA0) a serem enviados para o Aprovador.
User Function ZAPVDA0L(dSinceDate, nLastId, nLimit, nRefLastId, cRefError)
    Local aRecnos, aArea, cAlias, cSQL, aDoc, aIt, cTxtMsg, nPos
    Local aDocsRet := {}

    // Limpa cRefError
    cRefError := ""

    // Busca RECNOS pendentes da DA0
    aRecnos := fGetRecnos(dSinceDate, nLastId, nLimit, @nRefLastId)

    // Retorna se nao houver registros pendentes.
    If Len(aRecnos) == 0
        Return aDocsRet
    EndIf

    // Executa a query para buscar os dados das DA0s que estao pendentes de aprovacao.
    aArea  := GetArea()
    cAlias := GetNextAlias()

    cSQL := "SELECT DA0.R_E_C_N_O_ RECNODA0, DA0_FILIAL, DA0_CODTAB, DA0_DESCRI, DA0_DATDE, DA0_HORADE, DA0_DATATE, DA0_HORATE, DA0_CONDPG, DA0_ZAPROV,"
    cSQL += "       DA1_TIPPRE, DA1_ITEM, DA1_CODPRO, DA1_PRCVEN, DA1_DATVIG, "
    cSQL += "       B1_DESC, B1_UM, "
    cSQL += "       E4_CODIGO, E4_DESCRI "
    cSQL += "FROM " + RetSQLTab("DA0") + " "
    cSQL += "INNER JOIN " + RetSQLTab("DA1") + " "
    cSQL += "   ON DA1_FILIAL     = " + ValToSQL(xFilial("DA1"))
    cSQL += "  AND DA1_CODTAB     = DA0_CODTAB "
    cSQL += "  AND DA1_ATIVO      = '1' "
    cSQL += "  AND DA1.D_E_L_E_T_ = ' ' "
    cSQL += "INNER JOIN " + RetSQLTab("SB1") + " "
    cSQL += "   ON B1_FILIAL      = " + ValToSQL(xFilial("SB1"))
    cSQL += "  AND B1_COD         = DA1_CODPRO "
    cSQL += "  AND SB1.D_E_L_E_T_ = ' ' "
    cSQL += "LEFT JOIN " + RetSQLTab("SE4") + " "
    cSQL += "   ON E4_FILIAL      = " + ValToSQL(xFilial("SE4"))
    cSQL += "  AND E4_CODIGO      = DA0_CONDPG "
    cSQL += "  AND SE4.D_E_L_E_T_ = ' ' "
    cSQL += "WHERE DA0.R_E_C_N_O_ IN (" + StrTran(ArrTokStr(aRecnos), "|", ",")  + ") "
    cSQL += "ORDER BY DA0.R_E_C_N_O_"

    DBUseArea(.T., "TOPCONN", TCGenQry(,,cSQL), cAlias, .F., .T.)
    TCSetField(cAlias, "RECNODA0"  , "N", 10)
    TCSetField(cAlias, "DA0_DATDE" , "D")
    TCSetField(cAlias, "DA0_DATATE", "D")
    TCSetField(cAlias, "DA1_DATVIG", "D")
    TCSetField(cAlias, "DA1_PRCVEN", "N", 9, 2)

    Do While !(cAlias)->(EOF())

        // Verifica se registro ja existe na aDocsRet
        nPos := AScan(aDocsRet, {|x| x[1, 1] == (cAlias)->RECNODA0})

        If nPos == 0
            // Cria cada aDoc no formato documentado em <LINK_CENTRAL_AJUDA>      
            aDoc := {{}, {}, {}}

            cTxtMsg := "Filial: " + cFilAnt + ". Tabela de Preco: " + AllTrim((cAlias)->DA0_CODTAB)

            // aDoc[1]: Cabecalho
            aDoc[1] := {(cAlias)->RECNODA0,;                                          // 1  R_E_C_N_O_
                        cFilAnt,;                                                     // 2  FILIAL
                        "ZAPVDA0",;                                                   // 3  ID da customizacao no Aprovador Conector
                        {,, {AllTrim((cAlias)->DA0_ZAPROV)}},;                        // 4  array de aprovadores
                        "Protheus_ZAPVDA0",;                                          // 5  layoutId criado pelo Portal do Aprovador
                        "",;                                                          // 6  companyName 
                        FWTimeStamp(5, (cAlias)->DA0_DATDE, (cAlias)->DA0_HORADE),;   // 7  creationDate
                        "",;                                                          // 8  dueDate
                        "-",;                                                         // 9  textFrom
                        "Tabela de Pre�o",;                                           // 10 textSubject
                        cTxtMsg,;                                                     // 11 textMessage
                        "-",;                                                         // 12 textValue
                        " ",;                                                         // 13 labelValue
                        (cAlias)->DA0_CODTAB}                                         // 14 textId

            // aDoc[2]: Metadados do documento
            aDoc[2] := {{"RECNODA0"  ,,, cValToChar((cAlias)->RECNODA0)},;
                        {"DA0_FILIAL",,, AllTrim((cAlias)->DA0_FILIAL)},;
                        {"DA0_CODTAB",,, AllTrim((cAlias)->DA0_CODTAB)},;
                        {"DA0_DESCRI",,, AllTrim((cAlias)->DA0_DESCRI)},;
                        {"DA0_DATDE" ,,, cValToChar((cAlias)->DA0_DATDE)},;
                        {"DA0_HORADE",,, AllTrim((cAlias)->DA0_HORADE)},;
                        {"DA0_DATATE",,, cValToChar((cAlias)->DA0_DATATE)},;
                        {"DA0_HORATE",,, AllTrim((cAlias)->DA0_HORATE)},;
                        {"DA0_CONDPG",,, AllTrim((cAlias)->DA0_CONDPG)},;
                        {"E4_CODIGO" ,,, AllTrim((cAlias)->E4_CODIGO)},;
                        {"E4_DESCRI" ,,, AllTrim((cAlias)->E4_DESCRI)}}

            // Adiciona aDoc ao array de documentos
            AAdd(aDocsRet, aDoc)
        Else
            // Recupera aDoc ja criado na aDocsRet
            aDoc := aDocsRet[nPos]
        EndIf
        
        // Cria array com os dados da DA1 (produtos da tabela), que sera adicionado
        // como uma colecao do documento.
        aIt  := {""                           ,;                       // 1 collectionId (tera o valor default 'items')
                 AllTrim((cAlias)->DA1_CODPRO),;                       // 2 textId
                 AllTrim((cAlias)->B1_DESC)   ,;                       // 3 textName 
                 { {"DA1_TIPPRE",,, GetTIPPRE((cAlias)->DA1_TIPPRE)},; // 4 array de metadados do item 
                   {"DA1_ITEM"  ,,, AllTrim((cAlias)->DA1_ITEM)}    ,;
                   {"DA1_CODPRO",,, AllTrim((cAlias)->DA1_CODPRO)}  ,;
                   {"DA1_PRCVEN",,, AllTrim(Transform((cAlias)->DA1_PRCVEN, "@E 999,999,999.99"))},;
                   {"DA1_DATVIG",,, cValToChar((cAlias)->DA1_DATVIG)},;
                   {"B1_DESC"   ,,, AllTrim((cAlias)->B1_DESC)}      ,;
                   {"B1_UM"     ,,, AllTrim((cAlias)->B1_UM)} } }

        // Adiciona produto ao array de colecoes do documento
        AAdd(aDoc[3], aIt)

        // Passa para proximo registro
        (cAlias)->(DBSkip())
    EndDo

    RestArea(aArea)
Return aDocsRet

// Static Function para retornar os R_E_C_N_O_s dos registros que ser�o considerados.
//
// Parametros:
//   - dSinceDate.....: data a partir de quando as pendencias serao buscadas.
//   - nLastId........: [PAGINACAO] R_E_C_N_O_ da ultima SC retornada pela pesquisa anterior.
//   - nLimit.........: [PAGINACAO] Quantidade maxima de registros a serem retornados.
//   - @nRefLastId....: [PAGINACAO] Deve ser setado com o ultimo RECNO encontrado na pesquisa.
//
//   - Return: array com R_E_C_N_O_s retornados pela query.
//
// NOTE: usamos uma query exclusiva para retornarmos os R_E_C_N_O_s devido a paginacao.
//
Static Function fGetRecnos(dSinceDate, nLastId, nLimit, nRefLastId)
    Local cSQL    := ""
    Local aArea   := GetArea()
    Local cAlias  := GetNextAlias()
    Local aRecnos := {}
    Local cLimit  := IIF( Empty(nLimit), "", " TOP " + cValToChar(nLimit) + " " )

    Default nLastId    := 0
    Default nRefLastId := 0

    
    cSQL := " SELECT " + cLimit + "  R_E_C_N_O_ RECNODA0 FROM " + RetSQLTab("DA0")
    cSQL += " WHERE DA0_FILIAL  = '" + xFilial("DA0") + "'"
    cSQL += "   AND R_E_C_N_O_ > "   + cValToChar(nLastId) 
    cSQL += "   AND DA0_ZSTATU  = ' '" // Pendente
    cSQL += "   AND D_E_L_E_T_  = ' '
    cSQL += " ORDER BY R_E_C_N_O_"

    DBUseArea(.T., "TOPCONN", TCGENQRY(,,cSQL), cAlias, .F., .T.)
    TCSetfield(cAlias, "RECNODA0", "N", 10, 0)        

    If !(cAlias)->(EOF())
        Do while !(cAlias)->(EOF())
            AAdd(aRecnos, (cAlias)->RECNODA0)
            (cAlias)->(DBSkip())
        EndDo
    EndIF

    If Len(aRecnos) > 0
        nRefLastId := aRecnos[Len(aRecnos)]
    EndIf

    RestArea(aArea)
Return aRecnos

// Static Function para retornar a descricao do campo DA1_TIPPRE
//
// Parametros:
//   - cTIPPRE.....: valor do campo DA1_TIPPRE.
Static Function GetTIPPRE(cTIPPRE) 
    If cTIPPRE == "1"
        Return "1=Pre�o de Venda"
    ElseIf cTIPPRE == "2"
        Return "2=Venda Consumidor"
    ElseIf cTIPPRE == "3"
        Return "3=Atacado"
    ElseIf cTIPPRE == "4"
        Return "4=Varejo"
    ElseIf cTIPPRE == "5"
        Return "5=Promo��o"
    EndIf
Return ""

// User Function chamada pelo Aprovador para processar as aprovacoes.
//
// - Deve ser parametrizada no Aprovador Conector.
//
// Parametros:
//   - aDoc: array com informacoes do documento a ser processado:
//       [1]: R_E_C_N_O_
//       [2]: FILIAL
//       [3]: Array aUser:
//         [1]: ID do usuario com 6 caracteres
//         [2]: Login do usuario
//       [4]: String contendo o status com um dos valores a seguir:
//         "approved": documento aprovado
//         "rejected": documento reprovado
//       [5]: String contendo a observa��o digitada na aprovacao/reprovacao
//   - cRefError: se ocorrer alum erro no processamento, deve ser preenchida, pois sera
//                passado como referencia (@).
//                Este erro sera exibido para o usuario no Aprovador.
User Function ZAPVDA0P(aDoc, cRefError)
    Local aArea := GetArea()

    // Limpa cRefError
    cRefError := ""

    // Efetuamos as validacoes e atualizamos o registro como aprovado/reprovado.
    //
    // NOTE: poderiamos criar uma outra User Function contendo apenas a regra de aprovacao,
    //       para poder reutiliza-la caso a aprovacao tambem possa ser feita dentro do Protheus.

    DBSelectArea("DA0")
    DA0->(DBGoTo(aDoc[1]))

    If DA0->(EOF()) .OR. DA0->(Deleted())
        cRefError := "Registro DA0 nao encontrado ou deletado."
    ElseIf ! Empty(DA0->DA0_ZSTATU)
        cRefError := "Tabela de Preco ja " 
        cRefError += IIF(DA0->DA0_ZSTATU == "A", "aprovada", "reprovada") + "."
    Else
        Begin Transaction
            If ! RecLock("DA0", .F.)
                cRefError := "Registro DA0 em uso por outro usuario."
                Break
            EndIf

            // Seta o status
            If aDoc[4] == "approved"
                DA0->DA0_ZSTATU = "A"
            ElseIf aDoc[4] == "rejected"
                DA0->DA0_ZSTATU = "R"
            EndIf

            // Seta a observacao digitada.
            DA0->DA0_ZOBSM = aDoc[5] + CRLF + "- Enviado via Aprovador"

            DA0->(MsUnlock())
        End Transaction
    EndIf

    DA0->(DBCloseArea())
    RestArea(aArea)
Return

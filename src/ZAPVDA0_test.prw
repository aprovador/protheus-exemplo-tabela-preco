#include "Totvs.ch"
#include "testsuite.ch"

/*
 * Testes unitarios do programa ZAPVDA0.prw usando a lib https://github.com/nginformatica/advpl-testsuite
 *
 * NOTE: Este programa tem apenas o proposito de validar o funcionamento do exemplo fornecido
 *       por este repositorio. 
 *       NAO � NECESSARIO INCLUI-LO NA SUA CUSTOMIZACAO.
 */

TestSuite TZAPVDA0 Description "ZAPVDA0: Exemplo de aprovacao especifica de tabela de preco no Aprovador" Verbose
    Enable Environment "99" "01" USER "admin" PASSWORD ""
    Feature ZAPVDA0L Description "ZAPVDA0L: Lista pendencias"
    Feature ZAPVDA0P Description "ZAPVDA0P: Processa aprovacoes"
EndTestSuite

Feature ZAPVDA0L TestSuite TZAPVDA0
    Local dSince := CtoD("01/01/2020")
    Local cError := ""
    Local nLastId, aDocs, nI, aAux

    aDocs := U_ZAPVDA0L(dSince, 0, 10, @nLastId, @cError)

    ::Expect(Len(aDocs)):ToBe(2) // qtde de pendencias encontradas
    ::Expect(nLastId):ToBe(2)   // RECNO da ultima pendencia

    // Testa estrutura dos documentos retornados
    For nI := 1 To Len(aDocs)
        aAux := aDocs[nI]
        ::Expect(aAux[1, 3]):ToBe("ZAPVDA0")
        ::Expect(aAux[1, 4]):ToBe('{  ,  , {"gerente"} }')
        ::Expect(aAux[1, 10]):ToBe("Tabela de Pre�o")
        ::Expect(Len(aAux[1])):ToBe(14)      // qtde de elementos do cabecalho
        ::Expect(Len(aAux[2])):ToBe(11)      // qtde de metadados
        ::Expect(Len(aAux[3]) > 0):ToBe(.T.) // criou array de itens
        ::Expect(aAux[3, 1]):ToHaveType("A") // 1o item eh um array
        ::Expect(Len(aAux[3, 1, 4])):ToBe(7) // qtde de metadados por item
    Next

Return

Feature ZAPVDA0P TestSuite TZAPVDA0
    Local aDoc, cError

    // Testa aprovacao 
    aDoc := {1, "01", {"000003", "gerente"}, "approved", "Ok, aprovado"}
    U_ZAPVDA0P(aDoc, @cError)
    ::Expect(cError):ToBe("")
    ::Expect(GetDA0("001")[1]):ToBe("A")
    ::Expect(GetDA0("001")[2]):ToBe("Ok, aprovado" + CRLF + "- Enviado via Aprovador")

    // Testa reaprovacao
    aDoc := {2, "01", {"000003", "gerente"}, "rejected", "NOK"}
    U_ZAPVDA0P(aDoc, @cError)
    ::Expect(cError):ToBe("")
    ::Expect(GetDA0("002")[1]):ToBe("R")
    ::Expect(GetDA0("002")[2]):ToBe("NOK" + CRLF + "- Enviado via Aprovador")

    // Teste de erro
    aDoc := {999, "01", {"000003", "gerente"}, "approved", "Ok, aprovado"}
    U_ZAPVDA0P(aDoc, @cError)
    ::Expect(cError):ToBe("Registro DA0 nao encontrado ou deletado.")
Return

CompileTestSuite TZAPVDA0

Static Function GetDA0(cCodTab)
Return GetAdvFVal("DA0", {"DA0_ZSTATU", "DA0_ZOBSM", "DA0_ZAPROV"}, xFilial("DA0")+cCodTab, 1, {"", "", ""})
